fetch('https://restcountries.eu/rest/v2/all').then((res) => {
  res.json().then((data) => {
    // console.log(data);
    var allCountries = filteredData(data);
    showCountries(allCountries);
  });
});

// Declaracoes iniciais
let favCountries = [];
const listAllCountries = document.querySelector('#allCountries');
const listFavCountries = document.querySelector('#favCountries');
let input1 = document.querySelector('#input1');
input1.addEventListener('keydown', (e) => {
  if (e.key === 'Enter') {
    // todos.push(e.target.value);

    // RenderListItems(todos);
    console.log(e.target.value);
    input1.value = '';
  }
});
let input2 = document.querySelector('#input2');

//Filtra os dados provenientes da API pública
const filteredData = (data) => {
  const newData = data.map((item) => {
    return { name: item.name, flag: item.flag, population: item.population, id: item.numericCode };
  });
  newData.sort(function (a, b) {
    if (a.name < b.name) {
      return -1;
    } else if (a.name > b.name) {
      return 1;
    }
    return 0;
  });
  return newData;
};

//mostra a lista com países
function showCountries(array) {
  listAllCountries.innerHTML = '';
  const countries = array.map((item) => {
    //addTextBox
    let textBox = insertTextBox(item);
    //add button to row
    const addButton = document.createElement('button');
    addButton.classList.add('addButton');
    const textButton = document.createTextNode('+');
    addButton.appendChild(textButton);
    addButton.addEventListener('click', () => {
      allCountries = removeFavorite(array, item);
      showCountries(allCountries);
      favCountries = addFavorite(favCountries, item);
      showFavCountries(favCountries);
    });

    //add image to row
    let flag = insertFlag(item);
    //create textBox for the texts

    const li = document.createElement('li');
    li.classList.add('li');

    li.appendChild(addButton);
    li.appendChild(flag);
    li.appendChild(textBox);
    listAllCountries.appendChild(li);
  });
}

//mostra lista com países favoritos
function showFavCountries(array) {
  listFavCountries.innerHTML = '';
  let countries = array.map((item) => {
    //add TextBox
    let textBox = insertTextBox(item);
    //add button to row
    let addButton = insertButton();
    addButton.addEventListener('click', () => {
      favCountries = removeFavorite(array, item);
      showFavCountries(favCountries);
      allCountries = addFavorite(allCountries, item);
      showCountries(allCountries);
    });

    //add image to row
    let flag = insertFlag(item);
    const li = document.createElement('li');

    li.classList.add('li');

    li.appendChild(addButton);
    li.appendChild(flag);
    li.appendChild(textBox);
    listFavCountries.appendChild(li);
  });
}

const insertTextBox = (item) => {
  const name = document.createTextNode(`${item.name}`);
  const nameE = document.createElement('p');
  nameE.appendChild(name);
  const population = document.createTextNode(`${item.population}`);
  const popE = document.createElement('p');
  popE.appendChild(population);
  const textBox = document.createElement('div');
  textBox.classList.add('textBox');
  textBox.appendChild(nameE);
  // textBox.appendChild(popE);

  return textBox;
};

const insertButton = () => {
  const addButton = document.createElement('button');
  addButton.classList.add('deleteButton');
  const textButton = document.createTextNode('-');
  addButton.appendChild(textButton);

  return addButton;
};

const insertFlag = (item) => {
  const flag = document.createElement('img');
  flag.src = `${item.flag}`;
  flag.classList.add('img');
  return flag;
};

const removeFavorite = (array, item) => {
  const all = array.filter((value) => value.id !== item.id);
  all.sort(function (a, b) {
    if (a.name > b.name) {
      return 1;
    } else if (a.name < b.name) {
      return -1;
    } else {
      return 0;
    }
  });
  return all;
};

const addFavorite = (array, item) => {
  array.push(item);
  array.sort(function (a, b) {
    if (a.name < b.name) {
      return -1;
    } else if (a.name > b.name) {
      return 1;
    }
    return 0;
  });
  return array;
};
